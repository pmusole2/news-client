import {
  GET_ALL_ARTICLES,
  GET_ALL_ARTICLES_FAILED,
  GET_BY_CATEGORY,
  GET_BY_CATEGORY_FAILED,
  SET_ARTICLE_LOADING,
} from '../Types'

const initialState = {
  articlesLoading: true,
  articles: null,
  errors: null,
  message: null,
}

export default (state = initialState, action) => {
  const { type, payload } = action
  switch (type) {
    case GET_ALL_ARTICLES:
    case GET_BY_CATEGORY:
      return {
        ...state,
        articles: payload,
        errors: null,
        message: null,
        articlesLoading: false,
      }
    case GET_ALL_ARTICLES_FAILED:
    case GET_BY_CATEGORY_FAILED:
      return {
        ...state,
        articles: null,
        errors: payload,
        message: payload,
        articlesLoading: false,
      }
    case SET_ARTICLE_LOADING:
      return {
        ...state,
        articlesLoading: true,
      }
    default:
      return state
  }
}
