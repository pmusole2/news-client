import {
  CLEAR_AUTH_ERROR,
  LOGOUT,
  SET_AUTH_LOADING,
  SIGNIN_FAILED,
  SIGNIN_SUCCESS,
  SIGNUP_FAILED,
  SIGNUP_SUCCESS,
} from '../Types'

const { AsyncStorage } = require('react-native')

const initialState = {
  isAuthenticated: false,
  token: AsyncStorage.getItem('token'),
  user: null,
  error: null,
  authLoading: true,
}

export default (state = initialState, action) => {
  const { type, payload } = action
  switch (type) {
    case SET_AUTH_LOADING:
      return {
        ...state,
        authLoading: true,
      }
    case LOGOUT:
      return {
        ...state,
        isAuthenticated: false,
        token: null,
        user: null,
        authLoading: false,
      }
    case SIGNIN_SUCCESS:
    case SIGNUP_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        user: payload.user,
        authLoading: false,
      }
    case SIGNIN_FAILED:
    case SIGNUP_FAILED:
      return {
        ...state,
        isAuthenticated: false,
        user: null,
        error: payload,
      }
    case CLEAR_AUTH_ERROR:
      return {
        ...state,
        error: null,
        authLoading: false,
      }
    default:
      return state
  }
}
