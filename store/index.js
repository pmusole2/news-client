import axios from 'axios'

import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import auth from './reducers/authReducer'
import articles from './reducers/articleReducers'
// import axios from 'axios'
import { AsyncStorage } from 'react-native'

export const requestUrl = axios.create({
  baseURL: 'https://news-app-server.herokuapp.com',
  // baseURL: 'http://6d20ce3a706d.ngrok.io',
  headers: {
    'Content-Type': 'application/json',
  },
})

const rootReducer = combineReducers({
  auth,
  articles,
})

export const store = createStore(rootReducer, applyMiddleware(thunk))

// Store Subscription Listener
let currentState = store.getState()

store.subscribe(() => {
  let previousState = currentState
  currentState = store.getState()
  if (previousState.auth.token !== currentState.auth.token) {
    const token = currentState.auth.token
    setAuthToken(token)
  }
})

requestUrl.interceptors.response.use(
  res => res,
  err => {
    if (err.response.status === 401) {
      store.dispatch({ type: 'LOGOUT' })
      console.log(err)
    }
    return Promise.reject(err)
  }
)

const setAuthToken = async token => {
  if (token) {
    requestUrl.defaults.headers.common['x-auth-token'] = token
    await AsyncStorage.setItem('token', token)
  } else {
    delete requestUrl.defaults.headers.common['x-auth-token']
    await AsyncStorage.removeItem('token')
  }
}
