import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import auth from './reducers/authReducer'
import articles from './reducers/articleReducers'
import axios from 'axios'
import { AsyncStorage } from 'react-native'
import { getCurrentUser } from './actions/authActions'
import { LOGOUT } from './Types'

const setAuthToken = async token => {
  if (token) {
    axios.defaults.headers.common['x-auth-token'] = token
    await AsyncStorage.setItem('token', token)
  } else {
    delete axios.defaults.headers.common['x-auth-token']
    await AsyncStorage.removeItem('token')
  }
}

const rootReducer = combineReducers({
  auth,
  articles,
})

export const store = createStore(rootReducer, applyMiddleware(thunk))

// Store Subscription Listener
let currentState = store.getState()

store.subscribe(() => {
  let previousState = currentState
  currentState = store.getState()
  if (previousState.auth.token !== currentState.auth.token) {
    const token = currentState.auth.token
    setAuthToken(token)
    store.dispatch(getCurrentUser())
  } else if (!currentState.auth.user) {
    store.dispatch({ type: LOGOUT })
  }
})
