// API CONFIG
import { requestUrl } from '../index'

import {
  GET_ALL_ARTICLES,
  GET_ALL_ARTICLES_FAILED,
  GET_BY_CATEGORY,
  GET_BY_CATEGORY_FAILED,
  SET_ARTICLE_LOADING,
} from '../Types'

// ACTIONS
// SET LOADING
const setArticleLoading = () => dispatch => {
  dispatch({
    type: SET_ARTICLE_LOADING,
  })
}

// GET ALL ARTICLES
export const getAllArticles = user => async dispatch => {
  dispatch(setArticleLoading())
  let body
  if (user) {
    body = JSON.stringify({ id: user._id })
    try {
      const res = await requestUrl.post('/api/articles/user', body)
      dispatch({
        type: GET_ALL_ARTICLES,
        payload: res.data,
      })
    } catch (error) {
      dispatch({
        type: GET_ALL_ARTICLES_FAILED,
        payload: error.response.data,
      })
    }
  } else if (!user) {
    try {
      const res = await requestUrl.post('/api/articles/user', body)
      dispatch({
        type: GET_ALL_ARTICLES,
        payload: res.data,
      })
    } catch (error) {
      dispatch({
        type: GET_ALL_ARTICLES_FAILED,
        payload: error.response.data,
      })
    }
  }
}

// GET BY CATEGORY
export const getArticleByCat = ({ selected, user }) => async dispatch => {
  dispatch(setArticleLoading())
  let body
  if (user) {
    body = JSON.stringify({ id: user._id, selected })
    try {
      const res = await requestUrl.post('/api/articles/user/selected', body)
      dispatch({
        type: GET_BY_CATEGORY,
        payload: res.data,
      })
    } catch (error) {
      dispatch({
        type: GET_BY_CATEGORY_FAILED,
        payload: error.response.data,
      })
    }
  } else if (!user) {
    body = JSON.stringify({ selected })
    try {
      const res = await requestUrl.post('/api/articles/user/selected', body)
      dispatch({
        type: GET_BY_CATEGORY,
        payload: res.data,
      })
    } catch (error) {
      dispatch({
        type: GET_BY_CATEGORY_FAILED,
        payload: error.response.data,
      })
    }
  }
}
