import { AsyncStorage } from 'react-native'
import { requestUrl } from '../index'

import {
  CLEAR_AUTH_ERROR,
  LOGOUT,
  SET_AUTH_LOADING,
  SIGNIN_SUCCESS,
  SIGNUP_FAILED,
  SIGNUP_SUCCESS,
} from '../Types'

const setAuthLoading = () => dispatch => {
  dispatch({ type: SET_AUTH_LOADING })
}

export const clearAuthError = () => dispatch => {
  dispatch({
    type: CLEAR_AUTH_ERROR,
  })
}

export const setAuthToken = async token => {
  if (token) {
    requestUrl.defaults.headers.common['x-auth-token'] = token
    await AsyncStorage.setItem('token', token)
  } else {
    delete requestUrl.defaults.headers.common['x-auth-token']
    await AsyncStorage.removeItem('token')
  }
}

// Get Current User Account
export const getCurrentUser = () => async dispatch => {
  try {
    const res = await requestUrl.get('/api/user')
    return res.data
  } catch (error) {
    console.log(error.message || error.response.data)
  }
}

export const logout = () => async dispatch => {
  await AsyncStorage.removeItem('token')
  dispatch({
    type: LOGOUT,
  })
}

// Signin Handler
export const signinHandler = inputs => async dispatch => {
  dispatch(setAuthLoading())
  const body = JSON.stringify(inputs)
  try {
    const res = await requestUrl.post('/api/user/auth', body)

    await setAuthToken(res.data.token)
    const user = await dispatch(getCurrentUser())
    dispatch({
      type: SIGNIN_SUCCESS,
      payload: { token: res.data, user: user },
    })
  } catch (error) {
    dispatch({
      type: SIGNUP_FAILED,
      payload: error,
    })
  }
}

// SIGNUP Handler
export const signupHandler = inputs => async dispatch => {
  dispatch(setAuthLoading())
  const body = JSON.stringify(inputs)
  try {
    const res = await requestUrl.post('/api/user/new', body)
    await setAuthToken(res.data.token)
    const user = await dispatch(getCurrentUser())
    dispatch({
      type: SIGNUP_SUCCESS,
      payload: { token: res.data, user: user },
    })
  } catch (error) {
    dispatch({
      type: SIGNUP_FAILED,
      payload: error,
    })
  }
}

// Autologin
export const autoLogin = () => async dispatch => {
  const token = await AsyncStorage.getItem('token')
  if (token) {
    await setAuthToken(token)
    const res = await dispatch(getCurrentUser())
    dispatch({
      type: SIGNIN_SUCCESS,
      payload: { token: token, user: res.data },
    })
  }
  if (!token) {
    dispatch({
      type: LOGOUT,
    })
  }
}
