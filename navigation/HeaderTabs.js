import React from 'react'

import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { colors } from '../constants/Styles'
import {
  getAllArticles,
  getArticleByCat,
} from '../store/actions/articleActions'

const Header = ({ selected, setSelected }) => {
  const dispatch = useDispatch()
  const user = useSelector(state => state.auth.user)

  return (
    <View style={styles.containerHeader}>
      <View style={styles.tabContainer}>
        <TouchableOpacity
          style={{
            backgroundColor: selected === 'All' && colors.green_two,
            padding: 15,
            margin: 0,
          }}
          onPress={() => {
            setSelected('All')
            dispatch(getAllArticles(user))
          }}>
          <View>
            <Text style={{ color: colors.white }}>All</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            backgroundColor: selected === 'Covid-19' && colors.green_two,
            padding: 15,
            margin: 0,
          }}
          onPress={() => {
            setSelected('Covid-19')
            dispatch(getArticleByCat({ selected: 'Covid-19', user }))
          }}>
          <View>
            <Text style={{ color: colors.white }}>Covid-19</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            backgroundColor: selected === 'Technology' && colors.green_two,
            padding: 15,
            margin: 0,
          }}
          onPress={() => {
            setSelected('Technology')
            dispatch(getArticleByCat({ selected: 'Technology', user }))
          }}>
          <View>
            <Text style={{ color: colors.white }}>Technology</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            backgroundColor: selected === 'Sports' && colors.green_two,
            padding: 15,
            margin: 0,
          }}
          onPress={() => {
            setSelected('Sports')
            dispatch(getArticleByCat({ selected: 'Sports', user }))
          }}>
          <View>
            <Text style={{ color: colors.white }}>Sports</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            backgroundColor: selected === 'Entertainment' && colors.green_two,
            padding: 15,
            margin: 0,
          }}
          onPress={() => {
            setSelected('Entertainment')
            dispatch(getArticleByCat({ selected: 'Entertainment', user }))
          }}>
          <View>
            <Text style={{ color: colors.white }}>Entertainment</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          style={{
            backgroundColor: selected === 'Subscribers' && colors.green_two,
            padding: 15,
            margin: 0,
          }}
          onPress={() => {
            setSelected('Subscribers')
            dispatch(getArticleByCat({ selected: 'Subscribers', user }))
          }}>
          <View>
            <Text style={{ color: colors.white }}>Subscribers</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            backgroundColor: selected === 'Free' && colors.green_two,
            padding: 15,
            margin: 0,
          }}
          onPress={() => {
            setSelected('Free')
            dispatch(getArticleByCat({ selected: 'Free', user }))
          }}>
          <View>
            <Text style={{ color: colors.white }}>Free</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  //   containerHeader: {
  //     flex: 1,
  //     flexDirection: 'column',
  //     justifyContent: 'center',
  //     alignItems: 'center',
  //   },
  textContainer: {
    marginTop: 70,
  },
  textWhite: {
    color: 'black',
    marginVertical: 10,
  },
  tabContainer: {
    backgroundColor: colors.dark_one,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    // paddingHorizontal: 5,
    height: '20%',
    alignItems: 'center',
    // marginTop: 10,
    height: 40,
    borderBottomWidth: 2,
    borderBottomColor: colors.green_two,
  },
})
export default Header
