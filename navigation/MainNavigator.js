import React from 'react'
import { SafeAreaView, Text, TouchableOpacity, View } from 'react-native'
import { EvilIcons, FontAwesome5, MaterialIcons } from '@expo/vector-icons'
import { createStackNavigator } from '@react-navigation/stack'
import {
  createDrawerNavigator,
  DrawerItem,
  DrawerItemList,
} from '@react-navigation/drawer'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import SigninScreen, {
  signinScreenOptions,
} from '../components/screens/AuthenticationScreens/SigninScreen'
import SignupScreen, {
  signupScreenOptions,
} from '../components/screens/AuthenticationScreens/SignupScreen'
import { Button, Icon } from 'react-native-elements'
import { colors } from '../constants/Styles'
import ArticleOverViewScreen, {
  articleOverviewOptions,
} from '../components/screens/articles/ArticleOverViewScreen'
import ArticleDetailScreen from '../components/screens/articles/ArticleDetailScreen'
import AccountScreen, {
  accountScreenOptions,
} from '../components/screens/account/AccountScreen'
import SettingsScreen, {
  settingScreenOptions,
} from '../components/screens/settings/SettingsScreen'
import { useDispatch, useSelector } from 'react-redux'
import CustomIcon from '../components/ui-components/CustomIcon'
import FlutterwavePayment from '../components/screens/AuthenticationScreens/FlutterwavePayment'
import { store } from '../store'
import { logout } from '../store/actions/authActions'

const dfNavigationOptions = {
  headerTitleAlign: 'center',
  headerStyle: {
    backgroundColor: colors.dark_one,
  },
  headerTintColor: colors.white,
}

const AuthStackNavigator = createStackNavigator()
const ArticleStackNavigator = createStackNavigator()
const MainDrawerNavigator = createDrawerNavigator()
const AccountStackNavigator = createStackNavigator()
const SettingsStackNavigator = createStackNavigator()

export const AuthNavigator = () => {
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated)
  return (
    <AuthStackNavigator.Navigator screenOptions={dfNavigationOptions}>
      {!isAuthenticated ? (
        <>
          <AuthStackNavigator.Screen
            name='Signin'
            component={SigninScreen}
            options={signinScreenOptions}
          />
          <AuthStackNavigator.Screen
            name='Signup'
            component={SignupScreen}
            options={signupScreenOptions}
          />
        </>
      ) : (
        <AuthStackNavigator.Screen
          name='Account'
          component={AccountNavigator}
          options={accountScreenOptions}
        />
      )}
    </AuthStackNavigator.Navigator>
  )
}

const ArticleNavigator = () => {
  return (
    <ArticleStackNavigator.Navigator screenOptions={dfNavigationOptions}>
      <ArticleStackNavigator.Screen
        name='Articles'
        component={ArticleOverViewScreen}
        options={articleOverviewOptions}
      />
      <ArticleStackNavigator.Screen
        name='ArticleDetail'
        component={ArticleDetailScreen}
        options={{
          headerTitle: 'Details',
        }}
      />
    </ArticleStackNavigator.Navigator>
  )
}

const AccountNavigator = () => {
  return (
    <AccountStackNavigator.Navigator screenOptions={dfNavigationOptions}>
      <AccountStackNavigator.Screen name='Account' component={AccountScreen} />
    </AccountStackNavigator.Navigator>
  )
}

const SettingsNavigator = () => {
  return (
    <SettingsStackNavigator.Navigator screenOptions={dfNavigationOptions}>
      <SettingsStackNavigator.Screen
        name='Settings'
        component={SettingsScreen}
        options={settingScreenOptions}
      />
    </SettingsStackNavigator.Navigator>
  )
}

export const DrawerNavigator = props => {
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated)
  const dispatch = useDispatch()
  return (
    <MainDrawerNavigator.Navigator
      drawerStyle={{ backgroundColor: 'rgba(32, 27, 36, 0.8)' }}
      drawerType='front'
      drawerContent={props => {
        // if (isAuthenticated) {
        return (
          <View style={{ flex: 1, paddingTop: 30 }}>
            <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
              <DrawerItemList {...props} />
              <Button
                title='Log Out'
                buttonStyle={{ margin: 8, backgroundColor: colors.green_two }}
                disabled={!isAuthenticated}
                onPress={() => dispatch(logout())}
              />
            </SafeAreaView>
          </View>
        )
        // }
      }}
      drawerContentOptions={{
        activeBackgroundColor: colors.green_two,
        activeTintColor: 'white',
        inactiveBackgroundColor: colors.white,
        inactiveTintColor: 'gray',
        labelStyle: {
          //   textAlign: 'center',
          fontSize: 18,
        },
      }}>
      <MainDrawerNavigator.Screen
        name='Articles'
        component={ArticleNavigator}
        options={{
          drawerIcon: () => (
            <CustomIcon
              name='newspaper'
              Icon={FontAwesome5}
              color='gray'
              size={24}
            />
          ),
        }}
      />
      <MainDrawerNavigator.Screen
        name={isAuthenticated ? 'Account' : 'Sign in'}
        options={{
          drawerIcon: () => (
            <CustomIcon name='user' Icon={EvilIcons} color='gray' size={30} />
          ),
        }}
        component={AuthNavigator}
      />
      <MainDrawerNavigator.Screen
        name='Settings'
        component={SettingsNavigator}
        options={{
          drawerIcon: () => (
            <CustomIcon
              name='settings'
              Icon={MaterialIcons}
              color='gray'
              size={30}
            />
          ),
        }}
      />
    </MainDrawerNavigator.Navigator>
  )
}
