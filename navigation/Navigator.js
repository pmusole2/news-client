import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { AuthNavigator, DrawerNavigator } from './MainNavigator'

export const Navigator = () => {
  return (
    <NavigationContainer>
      <DrawerNavigator />
    </NavigationContainer>
  )
}
