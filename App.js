import React, { useEffect } from 'react'
import { StatusBar } from 'expo-status-bar'
import { Provider } from 'react-redux'
import { store } from './store/index'
import { Navigator } from './navigation/Navigator'
import { autoLogin } from './store/actions/authActions'

export default function App() {
  const authLoading = store.getState().auth.authLoading
  useEffect(() => {
    if (authLoading) {
      store.dispatch(autoLogin())
    }
  }, [])

  return (
    <>
      <Provider store={store}>
        <StatusBar style='light' />
        <Navigator />
      </Provider>
    </>
  )
}
