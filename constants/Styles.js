import { Dimensions, StyleSheet } from 'react-native'

export const colors = {
  mainLightBlue: '#12c0e3',
  lightBlue_two: 'rgb(73, 235, 232)',
  dark_one: 'rgb(32, 27, 36)',
  dark_two: 'rgb(12, 3, 46)',
  green_ish_one: 'rgb(29, 191, 159)',
  green_two: 'rgb(7, 235, 26)',
  white: 'rgb(255, 255, 255)',
  gradient_one:
    'linear-gradient(0deg, rgba(2,0,36,1) 0%, rgba(18,10,80,1) 47%, rgba(0,212,255,1) 100%)',
}

export const myStyles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get('screen').width,
    height: Dimensions.get('window').height,
    padding: 20,
    // marginTop: 20,
    backgroundColor: colors.dark_one,
  },
  cardStyles: {
    shadowColor: '#8ee4e8',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.46,
    shadowRadius: 11.14,
    elevation: 5,
    // borderRadius: 10,
    width: '91%',
    padding: 20,
    marginBottom: 10,
    backgroundColor: colors.dark_one,
    borderColor: 'gray',
  },
  titleStyles: {
    fontSize: 16,
  },
  articleStyle: {
    width: '90%',
  },
  text_items: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    // alignContent: 'space-between',
  },
})
