import { ToastAndroid } from 'react-native'

export const selectedArticle = (articles, selected) => {
  let newArt = articles.filter(article => {
    if (article.category === selected) {
      return article.category === selected
    } else if (
      article.category !== selected &&
      article.articleType === selected
    ) {
      return article.articleType === selected
    }
  })
  if (newArt) {
    return newArt
  } else {
    return new Array()
  }
}

// SIGNIN HANDLER
export const checkInputs = inputs => {
  if (
    !inputs.firstName ||
    !inputs.lastName ||
    !inputs.email ||
    !inputs.password ||
    !inputs.password2
  ) {
    ToastAndroid.show('Please enter all the fields', ToastAndroid.SHORT)
    return false
  } else if (inputs.password !== inputs.password2) {
    ToastAndroid.show('Passwords do not match', ToastAndroid.SHORT)
    return false
  }
  return true
}
