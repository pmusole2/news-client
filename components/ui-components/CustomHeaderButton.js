import React from 'react'
import { HeaderButton } from 'react-navigation-header-buttons'

const CustomHeaderButton = props => {
  return (
    <HeaderButton
      {...props}
      IconComponent={props.IconComponent}
      iconSize={props.iconSize}
      iconName={props.iconName}
      color={props.color}
    />
  )
}

export default CustomHeaderButton
