import React from 'react'

const CustomIcon = ({ name, size, color, Icon }) => {
  return <Icon name={name} size={size} color={color} />
}

export default CustomIcon
