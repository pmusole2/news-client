import React from 'react'
import { StyleSheet, ToastAndroid, View } from 'react-native'
import { PayWithFlutterwave } from 'flutterwave-react-native'

const FlutterwavePayment = () => {
  return (
    <View
      style={{
        flex: 1,
        width: 200,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <PayWithFlutterwave
        style={{ alignSelf: 'center', height: 32 }}
        onRedirect={res =>
          ToastAndroid.show(`Transaction ${res.status}`, ToastAndroid.SHORT)
        }
        options={{
          amount: 150,
          customer: {
            name: 'Prince Musole',
            email: 'pmusole2@gmail.com',
            phonenumber: '+260960505576',
          },
          currency: 'USD',
          tx_ref: '12345654321',
          payment_options: 'card,mobilemoneyzambia',
          authorization: 'FLWSECK_TEST-6752f72ccc4144e90ab0b12f0b6dc7d0-X',
        }}
      />
    </View>
  )
}

export default FlutterwavePayment

const styles = StyleSheet.create({})
