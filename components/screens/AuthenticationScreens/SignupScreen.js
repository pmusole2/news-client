import React, { useEffect, useState } from 'react'
import { Alert, Text, TouchableOpacity, View } from 'react-native'
import { Button, Card, Input } from 'react-native-elements'
import { Entypo, MaterialCommunityIcons, EvilIcons } from '@expo/vector-icons'
import { colors, myStyles } from '../../../constants/Styles'
import CustomIcon from '../../ui-components/CustomIcon'
import {
  clearAuthError,
  signupHandler,
} from '../../../store/actions/authActions'
import { useDispatch, useSelector } from 'react-redux'
import { checkInputs } from '../../../constants/Helpers'

const SignupScreen = ({ navigation }) => {
  const [inputs, setInputs] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    password2: '',
  })

  const { authLoading, error } = useSelector(state => state.auth)
  const dispatch = useDispatch()
  useEffect(() => {
    if (error) {
      Alert.alert(
        'Signup Error',
        'Could not sign up right now, Please try again',
        [{ text: 'Okay', onPress: () => dispatch(clearAuthError()) }]
      )
    }
  }, [error])
  return (
    <View style={myStyles.mainContainer}>
      <CustomIcon
        name='user'
        size={200}
        color={colors.white}
        Icon={EvilIcons}
      />
      <Card containerStyle={{ ...myStyles.cardStyles }}>
        <Input
          placeholder='First Name'
          onChangeText={e => setInputs({ ...inputs, firstName: e })}
          inputStyle={{
            // backgroundColor: 'white',
            color: 'white',
          }}
          placeholderTextColor={colors.white}
          leftIcon={
            <CustomIcon
              Icon={Entypo}
              name='user'
              size={24}
              color={colors.white}
            />
          }
        />
        <Input
          inputStyle={{
            // backgroundColor: 'white',
            color: 'white',
          }}
          placeholder='Last Name'
          onChangeText={e => setInputs({ ...inputs, lastName: e })}
          placeholderTextColor={colors.white}
          leftIcon={
            <CustomIcon
              Icon={Entypo}
              name='user'
              size={24}
              color={colors.white}
            />
          }
        />
        <Input
          inputStyle={{
            // backgroundColor: 'white',
            color: 'white',
          }}
          placeholder='Email Address'
          keyboardType='email-address'
          onChangeText={e => setInputs({ ...inputs, email: e })}
          placeholderTextColor={colors.white}
          leftIcon={
            <CustomIcon
              Icon={Entypo}
              name='mail'
              size={24}
              color={colors.white}
            />
          }
        />
        <Input
          inputStyle={{
            // backgroundColor: 'white',
            color: 'white',
          }}
          placeholder='Password'
          secureTextEntry
          onChangeText={e => setInputs({ ...inputs, password: e })}
          placeholderTextColor={colors.white}
          leftIcon={
            <CustomIcon
              Icon={MaterialCommunityIcons}
              name='textbox-password'
              size={24}
              color={colors.white}
            />
          }
        />
        <Input
          inputStyle={{
            // backgroundColor: 'white',
            color: 'white',
          }}
          placeholder='Confirm Password'
          secureTextEntry
          onChangeText={e => setInputs({ ...inputs, password2: e })}
          placeholderTextColor={colors.white}
          leftIcon={
            <CustomIcon
              Icon={MaterialCommunityIcons}
              name='textbox-password'
              size={24}
              color={colors.white}
            />
          }
        />
        <Button
          title={!authLoading ? 'Signup' : ''}
          type='outline'
          buttonStyle={{ borderColor: colors.white }}
          titleStyle={{ color: colors.white }}
          loading={authLoading}
          onPress={() => {
            const res = checkInputs(inputs)
            if (!res) {
              return false
            } else if (res) {
              dispatch(signupHandler(inputs))
            }
          }}
        />
        <TouchableOpacity onPress={() => navigation.replace('Signin')}>
          <Text
            style={{
              fontSize: 18,
              textAlign: 'center',
              marginTop: 20,
              padding: 5,
              color: colors.white,
            }}>
            Already have an account? Sign in here
          </Text>
        </TouchableOpacity>
      </Card>
    </View>
  )
}

export default SignupScreen

export const signupScreenOptions = {
  headerTitle: 'Signup for a new Account',
  headerTintColor: colors.white,
  headerShown: false,
}
