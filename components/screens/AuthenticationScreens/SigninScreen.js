import React, { useEffect, useState } from 'react'
import { Alert, Text, ToastAndroid, TouchableOpacity, View } from 'react-native'
import { Button, Card, Input } from 'react-native-elements'
import { Entypo, MaterialCommunityIcons, EvilIcons } from '@expo/vector-icons'
import { colors, myStyles } from '../../../constants/Styles'
import CustomIcon from '../../ui-components/CustomIcon'
import { useDispatch, useSelector } from 'react-redux'
import {
  clearAuthError,
  signinHandler,
} from '../../../store/actions/authActions'

const SigninScreen = ({ navigation }) => {
  const [inputs, setInputs] = useState({
    email: '',
    password: '',
  })
  const { authLoading, error } = useSelector(state => state.auth)
  const dispatch = useDispatch()
  useEffect(() => {
    if (error) {
      Alert.alert(
        'Signin Error',
        'Could not sign in right now, Please try again',
        [{ text: 'Okay', onPress: () => dispatch(clearAuthError()) }]
      )
    }
  }, [error])
  return (
    <View style={myStyles.mainContainer}>
      <CustomIcon
        name='user'
        Icon={EvilIcons}
        size={200}
        color={colors.white}
      />
      <Card containerStyle={{ ...myStyles.cardStyles }}>
        <Input
          placeholderTextColor='white'
          autoCapitalize='none'
          inputStyle={{
            color: colors.white,
          }}
          placeholder='Email Address'
          keyboardType='email-address'
          onChangeText={e => setInputs({ ...inputs, email: e })}
          leftIcon={
            <CustomIcon
              Icon={Entypo}
              name='mail'
              size={24}
              color={colors.white}
            />
          }
        />
        <Input
          placeholder='Password'
          inputStyle={{
            // backgroundColor: 'white',
            color: 'white',
          }}
          style
          secureTextEntry
          placeholderTextColor='white'
          onChangeText={e => setInputs({ ...inputs, password: e })}
          leftIcon={
            <CustomIcon
              Icon={MaterialCommunityIcons}
              name='textbox-password'
              size={24}
              color={colors.white}
            />
          }
        />
        <Button
          title={!authLoading ? 'Login' : ''}
          type='outline'
          buttonStyle={{ borderColor: colors.white }}
          titleStyle={{ color: colors.white }}
          onPress={() => {
            if (!inputs.email || !inputs.password) {
              ToastAndroid.show(
                'Please enter your login details',
                ToastAndroid.SHORT
              )
              return
            }
            dispatch(signinHandler(inputs))
          }}
          loading={authLoading}
        />
        <TouchableOpacity onPress={() => navigation.replace('Signup')}>
          <Text
            style={{
              fontSize: 18,
              textAlign: 'center',
              marginTop: 20,
              padding: 5,
              color: colors.white,
            }}>
            Don't have an account yet? Sign up now
          </Text>
        </TouchableOpacity>
      </Card>
      {/* <Button
        title='Init Call'
        buttonStyle={{ borderColor: colors.white, marginTop: 20 }}
        titleStyle={{ color: colors.white }}
        type='outline'
        onPress={() => navigation.navigate('Flutterwave')}
      /> */}
    </View>
  )
}

export default SigninScreen

export const signinScreenOptions = {
  headerTitle: 'Log into your Account',
  headerTintColor: colors.mainLightBlue,
  headerShown: false,
}
