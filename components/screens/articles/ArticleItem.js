import React from 'react'
import { Dimensions, StyleSheet, Text, View } from 'react-native'
import { Card } from 'react-native-elements'
import { colors, myStyles } from '../../../constants/Styles'
import CustomIcon from '../../ui-components/CustomIcon'
import { MaterialIcons, FontAwesome, Entypo } from '@expo/vector-icons'
import Moment from 'react-moment'

const ArticleItem = ({
  data: { articleType, postedAt, title, body, category, admin },
}) => {
  return (
    <View style={{ width: Dimensions.get('screen').width }}>
      <Card
        containerStyle={{
          ...myStyles.cardStyles,
        }}>
        <View>
          <Text
            style={{
              color: colors.white,
              fontSize: 20,
              margin: 3,
              textAlign: 'center',
            }}>
            {title}
          </Text>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
            <CustomIcon
              name='user-secret'
              Icon={FontAwesome}
              size={22}
              color='white'
            />
            <Text style={{ color: colors.white, fontSize: 16, margin: 3 }}>
              {'  '}Posted by: {admin.name}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
            <CustomIcon name='news' Icon={Entypo} size={20} color='white' />
            <Text style={{ color: colors.white, fontSize: 16, margin: 3 }}>
              {'  '}Category: {category}
            </Text>
          </View>
          <View
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <CustomIcon
                name='date-range'
                Icon={MaterialIcons}
                color='white'
                size={22}
              />
              <Text style={{ color: colors.white, fontSize: 16, margin: 3 }}>
                {'  '}
                Date Posted:{'  postedAt'}
              </Text>
            </View>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <CustomIcon
                name={articleType === 'Free' ? 'money-off' : 'attach-money'}
                color={articleType !== 'Free' ? 'red' : colors.green_two}
                size={24}
                Icon={MaterialIcons}
              />
              <Text
                style={{
                  color: articleType !== 'Free' ? 'red' : colors.green_two,
                  margin: 3,
                }}>
                {articleType}
              </Text>
            </View>
          </View>
        </View>
      </Card>
    </View>
  )
}

export default ArticleItem

const styles = StyleSheet.create({})
