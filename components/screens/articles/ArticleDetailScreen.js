import React from 'react'
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  ToastAndroid,
  View,
} from 'react-native'
import { Button, Card } from 'react-native-elements'
import { useSelector } from 'react-redux'
import { colors, myStyles } from '../../../constants/Styles'
import FlutterwavePayment from '../AuthenticationScreens/FlutterwavePayment'

const ArticleDetailScreen = ({ navigation, route, data }) => {
  const id = route.params._id
  const article = useSelector(state => state.articles.articles).find(
    article => article._id === id
  )
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated)
  const user = useSelector(state => state.auth.user)
  return (
    <View style={myStyles.mainContainer}>
      <Card containerStyle={{ ...myStyles.cardStyles, height: '93%' }}>
        <Card.Title h3 h3Style={{ color: colors.white }}>
          {article.title}
        </Card.Title>
        <Card.Divider />
        <View style={{ ...myStyles.text_items, marginBottom: 15 }}>
          <Text style={{ color: colors.white, fontSize: 18 }}>
            Posted by: {article.admin.name}{' '}
          </Text>
          <Text style={{ color: colors.white, fontSize: 18 }}>
            Posted on: 07/07/2020
          </Text>
        </View>
        <Text
          style={{ color: colors.white, fontSize: 18, textAlign: 'center' }}>
          Article Type:{' '}
          <Text
            style={{
              color: article.articleType === 'Free' ? colors.green_two : 'red',
            }}>
            {article.articleType}
          </Text>
        </Text>
        <Card.Divider />
        <ScrollView style={{ height: '74%' }}>
          <Text
            style={{
              color: colors.white,
              fontSize: 20,
              textAlign: 'center',
              marginBottom: 15,
            }}>
            {article.body}
          </Text>
        </ScrollView>
        <Card.Divider />
        {!isAuthenticated && !user && article.articleType !== 'Free' ? (
          <Button
            // disabled={!isAuthenticated}
            onPress={() => {
              if (!isAuthenticated) {
                ToastAndroid.show('Please Sign in', ToastAndroid.SHORT)
                navigation.toggleDrawer()
                return
              } else {
              }
            }}
            title='Login to view more'
            type='outline'
            buttonStyle={{
              borderColor: colors.white,
              padding: 5,
              marginBottom: 12,
            }}
            titleStyle={{ color: colors.white }}
          />
        ) : isAuthenticated &&
          user.accountStatus === 'Free' &&
          article.articleType !== 'Free' ? (
          <View style={{ alignItems: 'center', marginVertical: 8 }}>
            <FlutterwavePayment />
          </View>
        ) : null}
      </Card>
    </View>
  )
}

export default ArticleDetailScreen

const styles = StyleSheet.create({})
