import React, { useEffect, useState } from 'react'
import {
  ActivityIndicator,
  FlatList,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { MaterialIcons } from '@expo/vector-icons'
import { HeaderButtons, Item } from 'react-navigation-header-buttons'
// import { articles, selectedArticle } from '../../../constants/Helpers'
import { colors, myStyles } from '../../../constants/Styles'
import Header from '../../../navigation/HeaderTabs'
import { getAllArticles } from '../../../store/actions/articleActions'
import ArticleItem from './ArticleItem'
import CustomHeaderButton from '../../ui-components/CustomHeaderButton'

const ArticleOverViewScreen = ({ navigation }) => {
  const [selected, setSelected] = useState(null)
  const { articles, articlesLoading } = useSelector(state => state.articles)
  const user = useSelector(state => state.auth.user)
  // const renderArticle = selectedArticle(articles, selected)
  const dispatch = useDispatch()

  useEffect(() => {
    setSelected('All')
    dispatch(getAllArticles(user))
  }, [])

  if (articlesLoading) {
    return (
      <View style={myStyles.mainContainer}>
        <ActivityIndicator size={50} color={colors.green_two} />
      </View>
    )
  }
  return (
    <>
      <Header selected={selected} setSelected={setSelected} />
      <SafeAreaView style={myStyles.mainContainer}>
        {articles && !articlesLoading ? (
          <FlatList
            data={articles}
            keyExtractor={item => item._id}
            renderItem={({ item }) => (
              <TouchableOpacity
                // style={{ width: '100%' }}
                onPress={() => {
                  navigation.navigate('ArticleDetail', { _id: item._id })
                }}>
                <ArticleItem data={item} />
              </TouchableOpacity>
            )}
          />
        ) : (
          <View style={myStyles.mainContainer}>
            <Text style={{ color: colors.white, fontSize: 20 }}>
              There are no articles for this category
            </Text>
          </View>
        )}
      </SafeAreaView>
    </>
  )
}

export default ArticleOverViewScreen

export const articleOverviewOptions = navData => {
  return {
    headerTitle: 'ARTICLES',
    headerLeft: () => (
      <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
        <Item
          title='Menu'
          iconName='menu'
          iconSize={24}
          IconComponent={MaterialIcons}
          color={colors.green_two}
          onPress={() => navData.navigation.toggleDrawer()}
        />
      </HeaderButtons>
    ),
  }
}
