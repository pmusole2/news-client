import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { colors, myStyles } from '../../../constants/Styles'
import { EvilIcons, MaterialIcons } from '@expo/vector-icons'
import { HeaderButtons, Item } from 'react-navigation-header-buttons'
import CustomHeaderButton from '../../ui-components/CustomHeaderButton'

const SettingsScreen = () => {
  return (
    <View style={myStyles.mainContainer}>
      <Text>Settings</Text>
    </View>
  )
}

export default SettingsScreen

const styles = StyleSheet.create({})

export const settingScreenOptions = navData => {
  return {
    headerTitle: 'Settings',
    headerLeft: () => (
      <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
        <Item
          title='Menu'
          iconName='menu'
          iconSize={24}
          IconComponent={MaterialIcons}
          color={colors.green_two}
          onPress={() => navData.navigation.toggleDrawer()}
        />
      </HeaderButtons>
    ),
  }
}
