import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Card } from 'react-native-elements'
import { useSelector } from 'react-redux'
import { colors, myStyles } from '../../../constants/Styles'
import CustomIcon from '../../ui-components/CustomIcon'
import { EvilIcons, MaterialIcons } from '@expo/vector-icons'
import { HeaderButtons, Item } from 'react-navigation-header-buttons'
import CustomHeaderButton from '../../ui-components/CustomHeaderButton'

const AccountScreen = () => {
  const { user, authLoading } = useSelector(state => state.auth)

  return (
    <View style={myStyles.mainContainer}>
      <CustomIcon
        name='user'
        Icon={EvilIcons}
        size={200}
        color={colors.white}
      />
      {user && !authLoading ? (
        <Card containerStyle={myStyles.cardStyles}>
          <Card.Title
            h3
            h3Style={{
              color: colors.white,
            }}>{`${user.firstName} ${user.lastName}`}</Card.Title>
          <Card.Divider />
          <View
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ color: colors.white, fontSize: 19, margin: 3 }}>
              Email: {user.email}{' '}
            </Text>
          </View>
          <View
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ color: colors.white, fontSize: 19, margin: 3 }}>
              Account Type:{' '}
              <Text
                style={{ color: colors.green_two, fontSize: 19, margin: 3 }}>
                {user.accountStatus}
              </Text>{' '}
            </Text>
          </View>
          <View
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ color: colors.white, fontSize: 19, margin: 3 }}>
              Account Creation Date:{' '}
            </Text>
          </View>
        </Card>
      ) : (
        <View>
          {' '}
          <Text>No User Data</Text>{' '}
        </View>
      )}
    </View>
  )
}

export default AccountScreen

export const accountScreenOptions = navData => {
  return {
    headerTitle: 'Account',
    headerLeft: () => (
      <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
        <Item
          title='Menu'
          iconName='menu'
          iconSize={24}
          IconComponent={MaterialIcons}
          color={colors.green_two}
          onPress={() => navData.navigation.toggleDrawer()}
        />
      </HeaderButtons>
    ),
  }
}
